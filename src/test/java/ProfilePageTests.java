import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProfilePageTests extends BaseTestClass {
    @Before
    public void setup(){
        String url = "http://practica.wantsome.ro/shop/?page_id=11";
        driver.get(url);
        loginValidUser();
    }


    @Test
    public void replaceFirstNameTest(){
        ProfilePageFactory profilePageFactory = new ProfilePageFactory(driver);
        profilePageFactory.replaceFirstNameAddressDetails();
        Assert.assertTrue(profilePageFactory.validMessage.isDisplayed());
    }

    @Test
    public void emptyEmailAddressTest(){
        ProfilePageFactory profilePageFactory = new ProfilePageFactory(driver);
        profilePageFactory.emptyEmailAddressAddressDetails();
        Assert.assertTrue(profilePageFactory.errorMessage.isDisplayed());
    }

    @Test
    public void emptyFirstNameBillingAddressTest(){
        ProfilePageFactory profilePageFactory = new ProfilePageFactory(driver);
        profilePageFactory.emptyFirstNameBillingAddress();
        Assert.assertEquals(profilePageFactory.errorMessageEmptyFirstName.getText(),"First name is a required field.");
    }


    @Test
    public void emptyLastNameBillingAddressTest(){
        ProfilePageFactory profilePageFactory = new ProfilePageFactory(driver);
        profilePageFactory.emptyLastNameBillingAddress();
        Assert.assertEquals(profilePageFactory.errorMessageEmptyLastName.getText(),"Last name is a required field.");
    }

    @Test
    public void emptyAddressBillingAddressTest(){
        ProfilePageFactory profilePageFactory = new ProfilePageFactory(driver);
        profilePageFactory.emptyAddressBillingAddress();
        Assert.assertEquals(profilePageFactory.errorMessageEmptyAddress.getText(),"Address is a required field.");
    }


    @Test
    public void emptyCityBillingAddressTest(){
        ProfilePageFactory profilePageFactory = new ProfilePageFactory(driver);
        profilePageFactory.emptyCityBillingAddress();
        Assert.assertEquals(profilePageFactory.errorMessageEmptyCity.getText(),"Town / City is a required field.");
    }

    @Test
    public void emptyPostCodeBillingAddressTest(){
        ProfilePageFactory profilePageFactory = new ProfilePageFactory(driver);
        profilePageFactory.emptyPostCodeBillingAddress();
        Assert.assertEquals(profilePageFactory.errorMessageEmptyPostCode.getText(),"Postcode / ZIP is a required field.");
    }

    @Test
    public void emptyPhoneBillingAddressTest(){
        ProfilePageFactory profilePageFactory = new ProfilePageFactory(driver);
        profilePageFactory.emptyPhoneBillingAddress();
        Assert.assertEquals(profilePageFactory.errorMessageEmptyPhone.getText(),"Phone is a required field.");
    }

    @Test
    public void emptyEmailAddressBillingAddressTest(){
        ProfilePageFactory profilePageFactory = new ProfilePageFactory(driver);
        profilePageFactory.emptyEmailBillingAddress();
        Assert.assertEquals(profilePageFactory.errorMessageEmptyEmailAddress.getText(),"Email address is a required field.");

    }

    @Test
    public void replaceBillingAddressTest(){
        ProfilePageFactory profilePageFactory= new ProfilePageFactory(driver);
        profilePageFactory.replaceBillingAddress();
        Assert.assertTrue(profilePageFactory.messageReplaceBillingAddress.isDisplayed());
    }
}





