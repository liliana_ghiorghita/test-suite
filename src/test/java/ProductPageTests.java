import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProductPageTests extends BaseTestClass {

    @Test
    public void postCommentTest() throws InterruptedException {
        String url = "http://practica.wantsome.ro/shop/?page_id=11";
        driver.get(url);
        loginValidUser();
        ProductPageFactory productPageFactory
                = new ProductPageFactory(driver);
        productPageFactory.insertComment();
        Assert.assertTrue(productPageFactory.commentWaitingApproval.isDisplayed());
    }

    @Test
    public void redirectTitle() {
        String url = "http://practica.wantsome.ro/shop/?product=converse";
        driver.get(url);
        ProductPageFactory productPageFactory
                = new ProductPageFactory(driver);
        productPageFactory.redirectTitleLink();
        Assert.assertEquals(driver.getCurrentUrl(), "http://practica.wantsome.ro/shop/");

    }

    @Test
    public void verifyPDP() {
        String url = "http://practica.wantsome.ro/shop/?product=converse";
        driver.get(url);
        ProductPageFactory productPageFactory
                = new ProductPageFactory(driver);
        productPageFactory.verifyProductImage();
        Assert.assertTrue(productPageFactory.productDescription.isDisplayed());
        Assert.assertTrue(productPageFactory.productAddToCartButton.isDisplayed());

    }

    @Test
    public void removeProductFromCart() {
        String url = "http://practica.wantsome.ro/shop/?product=converse";
        driver.get(url);
        ProductPageFactory productPageFactory
                = new ProductPageFactory(driver);
        productPageFactory.addsProductToCartThenRemovesItAndWaits();
        Assert.assertTrue(productPageFactory.cartIsEmptyMessage.isDisplayed());

    }
}







