import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CheckoutPageTests extends BaseTestClass {
    @Before
    public void setup() {
    String url = "http://practica.wantsome.ro/shop/?page_id=11";
        driver.get(url);
    loginValidUser();}

    @Test
    public void successfulCheckoutTest() {
        ProductPageFactory productPageFactory
                = new ProductPageFactory(driver);
        productPageFactory.getProductPage();
        productPageFactory.productAddToCartButton.click();

        CheckoutPageFactory checkoutPageFactory
                = new CheckoutPageFactory(driver);
        checkoutPageFactory.successfulCheckout();
        Assert.assertEquals(checkoutPageFactory.orderReceivedMessage.getText(),"Thank you. Your order has been received.");
    }

}
