import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoryPageTests extends BaseTestClass {
    @Before
    public void setup() {
        String url = "http://practica.wantsome.ro/shop/?product_cat=men-collection";
        driver.get(url);
    }

    @Test
    public void sortPricesListTest() {
        CategoryPageFactory categoryPageFactory = new CategoryPageFactory(driver);
        categoryPageFactory.lowToHighPriceListAndWait();

        List<WebElement> newPriceList = driver.findElements(By.xpath("//span[@class='price']/*[not(self::del)]"));
        List<String> newPriceListSave = new ArrayList<>();
        for (WebElement e : newPriceList) {
            newPriceListSave.add(e.getText());
        }
        List<String> sortedPrices = new ArrayList<>(newPriceListSave);
        List<String> expectedList = Arrays.asList("5,00 lei", "20,00 lei", "25,00 lei", "25,00 lei", "30,00 lei", "35,00 lei", "40,00 lei", "45,00 lei", "50,00 lei", "60,00 lei");
        Assert.assertEquals(sortedPrices,expectedList);
    }

}
