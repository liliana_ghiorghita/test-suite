import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class BlogPageTests extends BaseTestClass {
    @Before
    public void setup() {
        String url = "http://practica.wantsome.ro/shop/?page_id=529";
        driver.get(url);
    }

    @Test
    public void numberOfArticlesTest() {
        List<WebElement> articlesCount = driver.findElements(By.cssSelector("div[class='entry-content-wrapper']"));
        Assert.assertEquals(articlesCount.size(), 10);
    }

    @Test
    public void verifyAuthorOfArticles() {
        List<WebElement> authorsOfArticles = driver.findElements(By.cssSelector("span[class='byline author vcard'] a"));
        for (WebElement author : authorsOfArticles) {
            Assert.assertEquals(author.getText(),"test test");
        }
    }
}

