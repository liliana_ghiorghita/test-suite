import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SearchPageTests extends BaseTestClass {

    @Before
    public void setup(){
        String url = "http://practica.wantsome.ro/shop/?page_id=11";
        driver.get(url);
    }


    @Test
    public void searchProductsTest() {
        SearchPageFactory searchPageFactory
                = new SearchPageFactory(driver);
        searchPageFactory.validSearch();
        Assert.assertTrue(searchPageFactory.searchResults.isDisplayed());
    }

    @Test
    public void invalidSearchResults(){
        SearchPageFactory searchPageFactory = new SearchPageFactory(driver);
        searchPageFactory.invalidSearch();
        Assert.assertEquals(searchPageFactory.errorMessage.getText(),"Sorry, but nothing matched your search terms. Please try again with some different keywords.");
    }
}







