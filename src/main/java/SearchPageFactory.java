import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPageFactory {
    private WebDriver driver;

    @FindBy(css = "#search-3>form>button")
    private WebElement searchButtonLink;

    @FindBy(css = "#search-3>form>input")
    private WebElement searchField;

    @FindBy(css = "#primary[class='content-area']")
    public WebElement searchResults;

    @FindBy(css = "#search-4 > form > input")
    private WebElement searchFieldFooter;

    @FindBy(css = "#search-4 > form > button > i")
    private WebElement searchButtonFooter;

    @FindBy(css = "#primary > section > div > p")
    public WebElement errorMessage;


    public SearchPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void validSearch(){
        searchField.sendKeys("Watch");
        searchButtonLink.click();
    }

    public void invalidSearch(){
        searchFieldFooter.sendKeys("@#$");
        searchButtonFooter.click();
    }
}
