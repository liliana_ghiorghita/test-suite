import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class CheckoutPageFactory {
    private WebDriver driver;

    @FindBy(id = "billing_first_name")
    public WebElement billingDetailsFirstName;

    @FindBy(id = "billing_last_name")
    public WebElement billingDetailsLastName;

    @FindBy(id = "ship-to-different-address-checkbox")
    public WebElement shipToDifferentAddressCheck;

    @FindBy(id = "billing_address_1")
    public WebElement billingDetailsAddress;

    @FindBy(id = "billing_city")
    public WebElement billingDetailsCity;

    @FindBy(id = "billing_postcode")
    public WebElement billingDetailsZip;

    @FindBy(id = "billing_phone")
    public WebElement billingDetailsPhone;

    @FindBy(css = "input[name='woocommerce_checkout_place_order']")
    public WebElement placeOrderButton;


    @FindBy(css = "p[class='woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received']")
    public WebElement orderReceivedMessage;

    public CheckoutPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private WebElement waitForElement(By by) {
        WebDriverWait wait =
                new WebDriverWait(driver, 12);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }


    public void successfulCheckout() {
        waitForElement(By.cssSelector("[id='woocommerce_widget_cart-1'] .checkout")).click();
        shipToDifferentAddressCheck.click();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", placeOrderButton);
        waitForElement(By.cssSelector("p[class='woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received']"));
    }


}
