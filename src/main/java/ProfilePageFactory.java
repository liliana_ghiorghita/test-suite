import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProfilePageFactory {
    private WebDriver driver;

    @FindBy(id = "locale")
    public WebElement languageDropdownLink;

    @FindBy(css = "div[class='woocommerce-message']")
    public WebElement validMessage;

    @FindBy(css = "li strong")
    public WebElement errorMessage;

    @FindBy(css = "#customer_login>div>form>p.form-row>input[name=login]")
    private WebElement loginLink;

    @FindBy(css = "#account_email")
    private WebElement emailAddressLink;

    @FindBy(css = "#account_first_name")
    private WebElement firstNameLink;

    @FindBy(css = "input[class='woocommerce-Button button']")
    private WebElement submitButtonLink;

    @FindBy(css = "li[class='woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account'] a")
    private WebElement accountDetailsLink;

    @FindBy(css = "#post-11 > div > div > div > div > div > nav > ul > li.woocommerce-MyAccount-navigation-link.woocommerce-MyAccount-navigation-link--edit-address > a")
    private WebElement adressLink;

    @FindBy(css = "#post-11 > div > div > div > div > div > div > div > div.u-column1.col-1.woocommerce-Address > header > a")
    private WebElement editBillingAddress;

    @FindBy(id = "billing_first_name")
    private WebElement firstNameBillingAddress;

    @FindBy(css = "#post-11 > div > div > div > div > div > div > form > div > p > input.button")
    private WebElement saveBillingAddressButton;

    @FindBy(css = " #post-11 > div > div > div > div > div > ul > li")
    public WebElement errorMessageEmptyFirstName;

    @FindBy(css = "#post-11 > div > div > div > div > div > ul > li")
    public WebElement errorMessageEmptyLastName;

    @FindBy(id = "billing_last_name")
    private WebElement lastNameBillingAddress;

    @FindBy(id = "billing_address_1")
    private WebElement addressBillingAddress;

    @FindBy(css = "#post-11 > div > div > div > div > div > ul > li")
    public WebElement errorMessageEmptyAddress;

    @FindBy(id = "billing_city")
    private WebElement cityBillingAddress;

    @FindBy(id = "billing_postcode")
    private WebElement postCodeBillingAddress;

    @FindBy(id = "billing_phone")
    private WebElement phoneBillingAddress;

    @FindBy(id = "billing_email")
    private WebElement emailBillingAddress;

    @FindBy(css = "#post-11 > div > div > div > div > div > ul > li")
    public WebElement errorMessageEmptyCity;

    @FindBy(css = "#post-11 > div > div > div > div > div > ul > li")
    public WebElement errorMessageEmptyPostCode;

    @FindBy(css = "#post-11 > div > div > div > div > div > ul > li")
    public WebElement errorMessageEmptyPhone;

    @FindBy(css = "#post-11 > div > div > div > div > div > ul > li")
    public WebElement errorMessageEmptyEmailAddress;

    @FindBy(css = "#post-11 > div > div > div > div > div > div.woocommerce-message")
    public WebElement messageReplaceBillingAddress;

    public ProfilePageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public void replaceFirstNameAddressDetails() {
        accountDetailsLink.click();
        Actions action = new Actions(driver);
        action.moveToElement(firstNameLink)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .sendKeys("Test First Name")
                .moveToElement(submitButtonLink)
                .click()
                .build()
                .perform();
    }

    public void emptyEmailAddressAddressDetails() {
        accountDetailsLink.click();
        Actions action = new Actions(driver);
        action.moveToElement(emailAddressLink)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .moveToElement(submitButtonLink)
                .click()
                .build()
                .perform();
    }

    public void emptyFirstNameBillingAddress() {
        adressLink.click();
        editBillingAddress.click();
        Actions action = new Actions(driver);
        action.moveToElement(firstNameBillingAddress)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .moveToElement(saveBillingAddressButton)
                .click()
                .build()
                .perform();
    }

    public void emptyLastNameBillingAddress() {
        adressLink.click();
        editBillingAddress.click();
        Actions action = new Actions(driver);
        action.moveToElement(lastNameBillingAddress)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .moveToElement(saveBillingAddressButton)
                .click()
                .build()
                .perform();
    }

    public void emptyAddressBillingAddress() {
        adressLink.click();
        editBillingAddress.click();
        Actions action = new Actions(driver);
        action.moveToElement(addressBillingAddress)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .moveToElement(saveBillingAddressButton)
                .click()
                .build()
                .perform();
    }

    public void emptyCityBillingAddress() {
        adressLink.click();
        editBillingAddress.click();
        Actions action = new Actions(driver);
        action.moveToElement(cityBillingAddress)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .moveToElement(saveBillingAddressButton)
                .click()
                .build()
                .perform();
    }

    public void emptyPostCodeBillingAddress() {
        adressLink.click();
        editBillingAddress.click();
        Actions action = new Actions(driver);
        action.moveToElement(postCodeBillingAddress)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .moveToElement(saveBillingAddressButton)
                .click()
                .build()
                .perform();
    }

    public void emptyPhoneBillingAddress() {
        adressLink.click();
        editBillingAddress.click();
        Actions action = new Actions(driver);
        action.moveToElement(phoneBillingAddress)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .moveToElement(saveBillingAddressButton)
                .click()
                .build()
                .perform();
    }

    public void emptyEmailBillingAddress() {
        adressLink.click();
        editBillingAddress.click();
        Actions action = new Actions(driver);
        action.moveToElement(emailBillingAddress)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .moveToElement(saveBillingAddressButton)
                .click()
                .build()
                .perform();
    }

    public void replaceBillingAddress() {
        adressLink.click();
        editBillingAddress.click();
        Actions action = new Actions(driver);
        action.moveToElement(firstNameBillingAddress)
                .click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .sendKeys("First Name")
                .moveToElement(lastNameBillingAddress).click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .sendKeys("Last Name")
                .moveToElement(addressBillingAddress).click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .sendKeys("Random Address")
                .moveToElement(cityBillingAddress).click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .sendKeys("Random City")
                .moveToElement(postCodeBillingAddress).click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .sendKeys("700126")
                .moveToElement(phoneBillingAddress).click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .sendKeys("0745666777")
                .moveToElement(emailBillingAddress).click()
                .sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a", Keys.DELETE))
                .sendKeys("lyly_here@gmail.com")
                .moveToElement(saveBillingAddressButton)
                .click()
                .build()
                .perform();
    }

}

