import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductPageFactory {
    private WebDriver driver;

    @FindBy(css = "i[class='fa fa-navicon']")
    private WebElement menuLink;

    @FindBy(css = "#menu-item-509 a")
    private WebElement categoryLink;

    @FindBy(css = "#primary > ul > li.post-171.product.type-product.status-publish.has-post-thumbnail.product_cat-men-collection.instock.sale.shipping-taxable.purchasable.product-type-simple > h3 > a")
    private WebElement productPageLink;

    @FindBy(css = "li[class='reviews_tab'] a")
    private WebElement reviewsTabLink;

    @FindBy(css = " a[class='star-4']")
    private WebElement starReviewsLink;

    @FindBy(id = "comment")
    private WebElement commentTextBoxLink;

    @FindBy(id = "submit")
    private WebElement submitButtonLink;

    @FindBy(css = "em[class='woocommerce-review__awaiting-approval']")
    public WebElement commentWaitingApproval;

    @FindBy(css = "#site-title > a")
    private WebElement titleLink;

    @FindBy(css = "figure[class='woocommerce-product-gallery__wrapper'] > div>a>img")
    public WebElement productImage;

    @FindBy(css = "div[class='woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab']>p")
    public WebElement productDescription;

    @FindBy(css = "button[class='single_add_to_cart_button button alt']")
    public WebElement productAddToCartButton;

    @FindBy(css = "td[class='product-remove']>a")
    public WebElement removeProductFromCartButton;

    @FindBy(css = "p[class='cart-empty']")
    public WebElement cartIsEmptyMessage;


    public ProductPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void insertComment() throws InterruptedException {
        menuLink.click();
        waitForElement(By.cssSelector("#menu-item-509 a")).click();
        productPageLink.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait
                .until(ExpectedConditions.elementToBeClickable(reviewsTabLink))
                .click();

        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", starReviewsLink);
        Thread.sleep(500);
        waitForElement(By.cssSelector(" a[class='star-4']"));
        starReviewsLink.click();
        String randomAlphabeticString = RandomStringUtils.randomAlphabetic(100);
        commentTextBoxLink.sendKeys(randomAlphabeticString);
        submitButtonLink.click();
    }

    private WebElement waitForElement(By by) {
        WebDriverWait wait =
                new WebDriverWait(driver, 12);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void redirectTitleLink() {
        titleLink.click();
    }

    public void getProductPage() {
        menuLink.click();
        waitForElement(By.cssSelector("#menu-item-509 a")).click();
        productPageLink.click();
    }

    public void verifyProductImage() {
        Object result = ((JavascriptExecutor) driver).executeScript(
                "return arguments[0].complete && " +
                        "typeof arguments[0].naturalWidth != \"undefined\" && " +
                        "arguments[0].naturalWidth > 0", productImage);

        boolean loaded = false;
        if (result instanceof Boolean) {
            loaded = (Boolean) result;
            System.out.println(loaded);
        }
    }

    public void addsProductToCartThenRemovesItAndWaits() {
        productAddToCartButton.click();
        driver.findElement(By.linkText("View cart")).click();
        removeProductFromCartButton.click();
        waitForElement(By.cssSelector("p[class='cart-empty']"));
    }
}
