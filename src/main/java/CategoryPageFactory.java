import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CategoryPageFactory {
    private WebDriver driver;

    @FindBy(xpath = "//span[@class='price']/*[not(self::del)]")
    private WebElement priceList;

    public CategoryPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void lowToHighPriceListAndWait() {
        Select sortDropdown = new Select(driver.findElement(By.cssSelector("#primary > form > select")));
        if (sortDropdown.getFirstSelectedOption().getText().contains("Default sorting")){
            sortDropdown.selectByVisibleText("Sort by price: low to high");
        }
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
}
