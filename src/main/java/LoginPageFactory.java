import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class LoginPageFactory {

    private WebDriver driver;

    @FindBy(id = "username")
    private WebElement userName;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(css = "input[class='woocommerce-Button button']")
    private WebElement loginBtn;

    public LoginPageFactory(WebDriver driver) {
        this.driver = driver;
    }

    public HomePageFactory authenticate(String user, String pass){
        userName.sendKeys(user);
        password.sendKeys(pass);
        loginBtn.click();
        return new HomePageFactory(driver);
    }
}
