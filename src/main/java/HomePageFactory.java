import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePageFactory {

    private WebDriver driver;

    @FindBy(css = "#menu-item-566 > a")
    private WebElement loginLink;

    @FindBy(css = "li[class='woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account'] a")
    private WebElement accountDetailsLink;

    public HomePageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public LoginPageFactory navigateToLoginPage() {
        loginLink.click();
        return new LoginPageFactory(driver);
    }

    public ProfilePageFactory navigateToProfilePageFactory() {
        WebDriverWait wait =
                new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("li[class='woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account'] a"))).click();
        return new ProfilePageFactory(driver);
    }
}